cmake_minimum_required(VERSION 3.12)

# Pull in SDK (must be before project)
include(pico_sdk_import.cmake)

project(tiny2040-mlx90614 C CXX ASM)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

# Initialize the SDK
pico_sdk_init()

add_executable(tiny2040-mlx90614
        src/mlx90614.c
)

# Pull in our pico_stdlib which pulls in commonly used features
target_link_libraries(tiny2040-mlx90614
  pico_stdlib
  hardware_i2c
  hardware_adc
)

pico_enable_stdio_usb(tiny2040-mlx90614 1)
pico_enable_stdio_uart(tiny2040-mlx90614 0)

# create map/bin/hex file etc.
pico_add_extra_outputs(tiny2040-mlx90614)

pico_set_program_url(tiny2040-mlx90614
  "https://gitlab.com/slimhazard/tiny2040-mlx90614"
)
pico_set_program_description(tiny2040-mlx90614
  "Control the Melexis MLX90614 contactless IR temperature sensor"
)

set(PROGRAM_VERSION "unknown")
find_program(GIT git)
if (GIT)
  execute_process(COMMAND ${GIT} describe --always
    OUTPUT_VARIABLE GIT_DESCRIBE_OUTPUT
    OUTPUT_STRIP_TRAILING_WHITESPACE
    RESULT_VARIABLE GIT_DESCRIBE_RESULT
  )
  if (GIT_DESCRIBE_RESULT EQUAL "0")
    set(PROGRAM_VERSION ${GIT_DESCRIBE_OUTPUT})
  else()
    message(WARNING "git describe returned ${GIT_DESCRIBE_RESULT}")
  endif()
else()
  message(WARNING "git not found")
endif()
pico_set_program_version(tiny2040-mlx90614 ${PROGRAM_VERSION})
