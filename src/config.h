/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#ifndef I2C_INST
#define I2C_INST PICO_DEFAULT_I2C_INSTANCE
#endif

#ifndef SDA_PIN
#define SDA_PIN PICO_DEFAULT_I2C_SDA_PIN
#endif

#ifndef SCL_PIN
#define SCL_PIN PICO_DEFAULT_I2C_SCL_PIN
#endif

#ifndef ADC_TEMP
#define ADC_TEMP (4)
#endif

#ifndef I2C_FREQ
#define I2C_FREQ (50 * 1000)
#endif

#ifndef MLX_ADDR
#define MLX_ADDR (0x5a)
#endif

static const char option_code[] = "BCC", temp_code = 'E';
