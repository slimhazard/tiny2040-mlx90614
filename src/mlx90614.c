/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "hardware/i2c.h"
#include "hardware/adc.h"

#include "config.h"
#include "cmds.h"
#include "crc8smbus.h"

#define MIN_RAW (0x2de4)
#define MAX_AMBIENT_RAW_E (0x45f3)
#define MAX_AMBIENT_RAW_K (0x4dc4)

#define SENSOR_BIT (6)

#define TIMEOUT_US (33 * 1000)

static float iir_vals[] = { 0.5, 0.25, 0.1666, 0.125, 1., 0.8, 0.666, 0.571 },
	gain_vals[] = { 1., 3., 6., 12.5, 25., 50., 100., 100. };

static const float adc_vref = 3.3f, vbe_ref = 0.706f, t_ref_c = 27.0f,
	m_v_per_c = -0.001721f;

static const int adc_bits = 12;
#define CONVERSION_FACTOR (adc_vref / (1 << adc_bits))

static inline double
raw2emissivity(uint16_t raw)
{
	return ((double)raw * (1/(double)UINT16_MAX));
}

static inline int
ta_in_range(uint16_t raw)
{
	if (raw < MIN_RAW)
		return (-1);
	if (temp_code == 'E' && raw > MAX_AMBIENT_RAW_E)
		return (1);
	if (temp_code == 'K' && raw > MAX_AMBIENT_RAW_K)
		return (1);
	return (0);
}

static inline int
to_in_range(uint16_t raw)
{
	if (raw < MIN_RAW)
		return (-1);
	return (0);
}

static inline float
raw2iir(uint16_t config)
{
	return (iir_vals[config & 0x07]);
}

static inline uint16_t
raw2fir(uint16_t config)
{
	return (8 << ((config >> 8) & 0x07));
}

static inline float
raw2gain(uint16_t config)
{
	return (gain_vals[(config >> 11) & 0x07]);
}

static inline int
raw2sensors(uint16_t config)
{
	return ((config & (1 << SENSOR_BIT)) + 1);
}

static uint16_t
read_mlx90614(const uint8_t cmd[])
{
	uint8_t buf[3];
	static uint8_t crcbuf[] = {
		MLX_ADDR << 1, 0, (MLX_ADDR << 1) + 1, 0, 0
	};
	int write_ret, read_ret;

	write_ret = i2c_write_timeout_us(I2C_INST, MLX_ADDR, cmd, 1, true,
					 TIMEOUT_US);
	read_ret = i2c_read_timeout_us(I2C_INST, MLX_ADDR, buf, sizeof(buf),
				       false, TIMEOUT_US);

	switch (write_ret) {
	case PICO_ERROR_GENERIC:
		puts("write: Address not acknowleged");
		return (0);
	case PICO_ERROR_TIMEOUT:
		puts("write: Timeout");
		return (0);
	default:
		if (write_ret != 1) {
			printf("%d bytes written\n", write_ret);
			return (0);
		}
	}

	switch (read_ret) {
	case PICO_ERROR_GENERIC:
		puts("read: Address not acknowleged");
		return (0);
	case PICO_ERROR_TIMEOUT:
		puts("read: Timeout");
		return (0);
	default:
		if (read_ret != sizeof(buf)) {
			printf("%d bytes read\n", read_ret);
			for (int i = 0; i < read_ret; i++)
				printf("0x%02x ", buf[i]);
			puts("");
			return (0);
		}
	}

	crcbuf[1] = cmd[0];
	crcbuf[3] = buf[0];
	crcbuf[4] = buf[1];
	if (crc8ccitt(crcbuf, 5) != buf[2]) {
		printf("CRC mismatch got:0x%02x expected:0x%02x\n", buf[2],
		       crc8ccitt(crcbuf, 5));
		return (0);
	}

	return ((buf[1] << 8) | buf[0]);
}

static void
print_temp(const char *s, uint16_t raw, bool obj)
{
	uint16_t fx_dK, fx_dC, fx_dF;
	int range;

	printf("%s:", s);
	if ((raw & 0x8000) != 0) {
		printf(" out of range (raw = 0x%04x)\n", raw);
		return;
	}
	if (obj)
		range = to_in_range(raw);
	else
		range = to_in_range(raw);
	if (range < 0) {
		printf(" below minimum (raw = 0x%04x)\n", raw);
		return;
	}
	if (range > 0) {
		printf(" above maximum (raw = 0x%04x)\n", raw);
		return;
	}

	/*
	 * The raw reading * 2 is degrees Kelvin * 100. So we compute
	 * with fixed-point arithmetic and scale to 1/100 for output.
	 */
	fx_dK = raw * 2;
	fx_dC = fx_dK - 27315;
	fx_dF = (uint16_t)(fx_dC * 1.8 + 0.5) + 3200;
	printf("\t%.2fK\t%.2fC\t%.2fF\n", fx_dK * 0.01, fx_dC * 0.01,
	       fx_dF * 0.01);
}

static inline float
adc_raw_2v(uint16_t raw)
{
        return (raw * CONVERSION_FACTOR);
}

static inline float
v2c(float v)
{
        return (t_ref_c - (v - vbe_ref) / -(m_v_per_c));
}

int main() {
	double emissivity = -1.;
	float iir, gain;
	uint8_t addr = 0;
	uint16_t response, cfg, fir, id[4] = { 0 };
	int sensors = 1;

	stdio_usb_init();

	// Use I2C1 on SDA and SCL pins 26 & 27 on a Tiny2040.
	i2c_init(I2C_INST, I2C_FREQ);
	gpio_set_function(SDA_PIN, GPIO_FUNC_I2C);
	gpio_set_function(SCL_PIN, GPIO_FUNC_I2C);
	gpio_pull_up(SDA_PIN);
	gpio_pull_up(SCL_PIN);

        adc_init();
        adc_set_temp_sensor_enabled(true);
        adc_select_input(ADC_TEMP);

	// Make the I2C pins available to picotool
	bi_decl(bi_2pins_with_func(SDA_PIN, SCL_PIN, GPIO_FUNC_I2C));

	do {
		response = 0;
		response = read_mlx90614(cfg_cmd);
		cfg = response;
	} while (response == 0);
	if (option_code[1] != 'C')
		sensors = raw2sensors(cfg);
	iir = raw2iir(cfg);
	fir = raw2fir(cfg);
	gain = raw2gain(cfg);

	while (emissivity == -1.) {
		response = read_mlx90614(emissivity_cmd);
		if (response == 0)
			continue;
		emissivity = raw2emissivity(response);
	}
	while (addr == 0) {
		response = read_mlx90614(smbus_addr_cmd);
		addr = (uint8_t)(response & 0x0ff);
	}
	for (int i = 0; i < 4; i++)
		do {
			id[i] = read_mlx90614(id_cmd[i]);
		} while (id[i] == 0);

	for (;;) {
		uint16_t response;
		float onboard_c;

		sleep_ms(1000);
		puts("");

		puts("Version: " PICO_PROGRAM_VERSION_STRING);
		printf("Emissivity: %.2f\n", emissivity);
		printf("Config raw: 0x%04x\n", cfg);
		printf("IIR: %.3f\n", iir);
		printf("FIR: %d\n", fir);
		printf("Gain: %.1f\n", gain);
		printf("SMBus address: 0x%02x\n", addr);
		printf("ID: %04x-%04x-%04x-%04x\n", id[0], id[1], id[2], id[3]);

		puts("");
		response = read_mlx90614(to1_cmd);
		if (response == 0)
			continue;
		print_temp("Object 1", response, true);

		if (sensors == 2) {
			response = read_mlx90614(to2_cmd);
			if (response == 0)
				continue;
			print_temp("Object 2", response, true);
		}

		response = read_mlx90614(ta_cmd);
		if (response == 0)
			continue;
		print_temp("Ambient", response, false);

		response = adc_read();
		onboard_c = v2c(adc_raw_2v(response));
		printf("Onboard:\t%.2fK\t%.2fC\t%.2fF\n", onboard_c + 273.15f,
		       onboard_c, (onboard_c * 1.8f) + 32);
	}
	return 0;
}
