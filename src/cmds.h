/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

/* EEPROM */
const uint8_t to_max_cmd[]	= { 0x20 },
	to_min_cmd[]		= { 0x21 },
	pwmctrl_cmd[]		= { 0x22 },
	ta_range_cmd[]		= { 0x23 },
	emissivity_cmd[]	= { 0x24 },
	cfg_cmd[]		= { 0x25 },
	smbus_addr_cmd[]	= { 0x2E },
	id_cmd[][1]		= { { 0x3C }, { 0x3D }, { 0x3E }, { 0x3F } };

/* RAM */
const uint8_t ir1_raw_cmd[]	= { 0x04 },
	ir2_raw_cmd[]		= { 0x05 },
	ta_cmd[]		= { 0x06 },
	to1_cmd[]		= { 0x07 },
	to2_cmd[]		= { 0x08 };

const uint8_t flags_cmd[] = { 0xf0 }, sleep_cmd[] = { 0xff };
